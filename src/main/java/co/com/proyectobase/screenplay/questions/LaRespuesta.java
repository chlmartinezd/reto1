package co.com.proyectobase.screenplay.questions;

import co.com.proyectobase.screenplay.userinterface.GoogleTraductorPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

public class LaRespuesta implements Question<String>{

	public static LaRespuesta es() {
		
		return new LaRespuesta();
	}

	@Override
	public String answeredBy(Actor andrea) {
		
		return Text.of(GoogleTraductorPage.AREA_TRADUCIDA).viewedBy(andrea).asString();
	}

}
