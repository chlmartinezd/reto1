package co.com.proyectobase.screenplay.userinterface;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.*;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;

@DefaultUrl ("http://demo.automationtesting.in/Register.html")
public class AutomationDemoPage extends PageObject {

	public static final Target FirstName = Target.the("campo nombre").located(By.xpath("//*[@id=\"basicBootstrapForm\"]/div[1]/div[1]/input"));
	//public static final Target LastName  = Target.the("campo apellido").located(By.id("source"));
	public static final Target LastName = Target.the("campo apellido").located(By.xpath("//*[@id=\"basicBootstrapForm\"]/div[1]/div[2]/input"));
	public static final Target Direccion = Target.the("campo direccion").located(By.xpath("//*[@id=\"basicBootstrapForm\"]/div[2]/div/textarea"));
	public static final Target Email = Target.the("campo email").located(By.xpath("//*[@id='eid']/input"));
	public static final Target Telefono = Target.the("campo telefono").located(By.xpath("//*[@id=\"basicBootstrapForm\"]/div[4]/div/input"));
	public static final Target rdbGenero = Target.the("campo genero").located(By.xpath( "//*[@id=\"basicBootstrapForm\"]/div[5]/div/label[2]/input"));
	public static final Target chk = Target.the("clic check").located(By.xpath( "//*[@id=\"basicBootstrapForm\"]/div[5]/div/label[2]/input"));
	public static final Target chk2 = Target.the("clic check2").located(By.id("checkbox2"));
	public static final Target Lenguaje = Target.the("campo lenguaje").located(By.id("msdd"));
	public static final Target Habilidades = Target.the("campo habilidades").located(By.xpath("//*[@id=\"Skills\"]"));
	public static final Target Countries = Target.the("campo Countries").located(By.xpath("//*[@id=\"countries\"]"));
	public static final Target Countries2 = Target.the("campo Countries2").located(By.xpath("//*[@id=\"countries\"]/option[12]"));
	public static final Target Year = Target.the("campo Año").located(By.id("yearbox"));
	public static final Target Mes = Target.the("campo Mes").located(By.xpath("//*[@id=\"basicBootstrapForm\"]/div[11]/div[2]/select"));
	public static final Target Dia = Target.the("campo Día").located(By.id("daybox"));
	public static final Target Password = Target.the("campo Contraseña").located(By.id("firstpassword"));
	public static final Target PasswordC = Target.the("campo ContraseñaC").located(By.id("secondpassword"));
	public static final Target Enviar = Target.the("Botón Enviar").located(By.id("submitbtn"));
	public static final Target Label_Confirmar= Target.the("Label para confirmar").located(By.xpath("//div//h4[1]"));
	

}
